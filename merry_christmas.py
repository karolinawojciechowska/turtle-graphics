from turtle import *
from time import sleep

bgcolor("#d5f5fe")
title("Merry Christmas")

tree = Turtle()
tree.shape("triangle")
tree.left(90)
tree.color("#b4d46e")
tree.speed(10)
tree.up()
tree.hideturtle()

r = 0
for i in range(1, 17):
    y = 30 * i
    for j in range(i - r):
        x = 30 * j
        tree.goto(x, -y + 280)  #right part of the tree
        tree.stamp()
        tree.goto(-x, -y + 280)  #left part of the tree
        tree.stamp()
    if i % 4 == 0:
        r += 2

tree.color("#9e6d30")
for i in range(17, 19):
    y = 30 * i
    for j in range(2):
        x = 30 * j
        tree.shape("square")
        tree.goto(x, -y + 280)
        tree.stamp()
        tree.goto(-x, -y + 280)
        tree.stamp()

positions = [(0, 280), (-120, 160), (120, 160), (-180, 40), (180, 40), (-240, -80), (240, -80), (-300, -200),
             (300, -200)]

for f in range(0, 9):
    bauble = Turtle()
    bauble.shape("circle")
    bauble.speed(10)
    bauble.color("#a52035")
    bauble.up()
    bauble.goto(positions[f])

text = Turtle()
text.up()
text.hideturtle()
text.shape("circle")
text.color("#b22323")
text.goto(0, -310)
text.write("Merry Christmas!!!", False, "center", font=("Arial", 24, "bold"))

sleep(20)